var changeNavResponsive = function() {
    var navElems = document.getElementsByTagName("nav");
    var nav = navElems[0];
    if (nav.className === "") {
        nav.className += " responsive";
    } else {
        nav.className = "";
    }
};

var changeAsideResponsive = function() {
    var asideElems = document.getElementsByTagName("aside");
    var aside = asideElems[0];
    if (aside.className === "") {
        aside.className += " responsive";
    } else {
        aside.className = "";
    }
};

document.addEventListener("DOMContentLoaded", function(event) {
    var btnMore = document.getElementById("more");
    btnMore.addEventListener("click", changeNavResponsive, false);

    var filters = document.getElementById("filters");
    filters.addEventListener("click", changeAsideResponsive, false);
});