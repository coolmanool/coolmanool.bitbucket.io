var changeNavStyle = function() {
    var x = document.getElementById("nav");
    if (x.className === "") {
        x.className += " column";
    } else {
        x.className = "";
    }
};

var changeAsideStyle = function() {
    var x = document.getElementById("aside");
    if (x.className === "") {
        x.className += "open";
    } else {
        x.className = "";
    }
};

document.addEventListener("DOMContentLoaded", function(event) {
    var navButton = document.getElementById("nav-button");
    navButton.addEventListener("click", changeNavStyle, false);

    var asideHeading = document.getElementById("aside-heading");
    asideHeading.addEventListener("click", changeAsideStyle, false);
});